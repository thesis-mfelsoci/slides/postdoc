(require 'org) ;; Org mode support
(require 'ox-publish) ;; publishing functions
(require 'ox-latex) ;; LaTeX publishing functions
(require 'org-ref) ;; bibliography support

;; Load presets.
(load-file "compose-publish/latex-publish.el")
(load-file "compose-publish/babel.el")

;; Override the default LaTeX publishing command.
(setq org-latex-pdf-process
      (list "latexmk --shell-escape -f -pdf -%latex -interaction=nonstopmode -output-directory=%o %f"))

;; Force publishing of unchanged files to make sure all the pages get published.
;; Otherwise, the files considered unmodified based on Org timestamps are not
;; published even if they were previously deleted from the publishing directory.
(setq org-publish-use-timestamps-flag nil)

;; Enable Babel code evaluation.
(setq org-export-babel-evaluate t)

;; Load languages for code block evaluation.
(org-babel-do-load-languages
 'org-babel-load-languages
 '((R . t)))

;; Include the Inria favicon in to the HTML header.
(setq org-html-head-extra "<link rel=\"icon\" type=\"image/x-icon\"
href=\"https://thesis-mfelsoci.gitlabpages.inria.fr/slides/postdoc/
favicon.ico\"/>")

;; Configure HTML website and PDF document publishing.
(setq org-publish-project-alist
      (list
       (list "postdoc"
             :base-directory "."
             :base-extension "org"
             :exclude ".*"
             :include ["postdoc.org"]
             :publishing-function '(org-beamer-publish-to-pdf-verbose-on-error)
             :publishing-directory "./public")
       (list "postdoc-tikz"
             :base-directory "./figures/tikz"
             :base-extension "tex"
             :publishing-function '(latex-publish-to-pdf)
             :publishing-directory "./public")
       (list "postdoc-figures"
             :base-directory "."
             :base-extension "org"
             :exclude ".*"
             :include ["postdoc.org"]
             :publishing-function '(org-babel-execute-file)
             :publishing-directory "./public")))

(provide 'publish)
