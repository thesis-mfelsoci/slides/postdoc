# Fast solvers for high-frequency aeroacoustics

[![pipeline status](https://gitlab.inria.fr/thesis-mfelsoci/slides/defense/badges/master/pipeline.svg)](https://gitlab.inria.fr/thesis-mfelsoci/slides/defense/-/commits/master)

[Slides](https://thesis-mfelsoci.gitlabpages.inria.fr/slides/defense/defense.pdf)

